# dcl-dao-data

This repository provides applications for interacting with Decentraland (DCL) DAO data available on Decentraland Catalyst nodes. The code found here is the same code running in production on behalf of the DAO providing open access to the data and analytics, but we've made the code open source so that you can see how it is done and replicate the data's collection if you wish or in the event that additional stewardship over the data is required.

The code is broken down into four different applications, each with their own installation and deployment process:

1. **DAO_DATA_COLLECT**: A set of microservices to collect data from each of the active DAO catalyst nodes. _Expects an external MongoDB cluster to store the data._
2. **DAO_DATA_ARCHIVE**: A set of microservices to archive data when the storage footprint of the data outgrows its capacity. _Expects an external AWS glacier instance._
3. **DAO_DATA_DERIVE**: A set of change-agent microservices to create derived metrics from the raw DAO data allowing more performant queries downstream.
4. **DAO_DATA_QUERY**: A set of microservices to provide a set of common queries and access to the DAO data to the general public.

Each of the above four applications can be run independently from each other, on separate infrastructure if desired. 

## DAO_DATA_COLLECT

The DAO_DATA_COLLECT application runs using docker-compose. You will need to install the latest version of docker to deploy this application.

The application deploys one container per catalyst node which will extract data from the `/comms/islands` endpoint from each Decentraland catalyst node.

### Installing DAO_DATA_COLLECT

The DAO_DATA_COLLECT application comes packaged with an installation routine. It can be run with or without using docker, but a docker-based installation is recommended as it offers standardization.
    
1. Copy the `example.env` in place file and rename it `.env`. Update each of the parameters in this file to the ones you will be using for your production application (e.g. your mongo instance details).

2a. The `install.py` python script can be run directly. 
    
* Make sure you install all required python libraries prior to execution using `pip3 install -r requirements.txt` or `pip install -r requirements` for older versions of python.
* Run the install script from the `DAO_DATA_COLLECT/` directory using `python3 install.py`

2b. The install script can alternatively be run using docker if you are unable to execute using your local python environment:

* Build the image using the command `docker build -t <image name> .` from the DCL_DATA_COLLECT directory.
* Run the docker image mounting the DCL_DATA_COLLECT directory to the `/usr/src/app` directory of the python image using the command `docker run -v /<path>/<to>/<DCL_DATA_COLLECT>/:/usr/src/app <image_name>` 
* [optional] delete the docker install image as you no longer need it.

If the script executed correctly, you should have an updated `docker-compose.yml` file, and `nodered/flows.json` & `nodered/flows_cred.json` files.

3. Build the node-red containers using `docker-compose build`

4. Start the containers using `docker-compose up`

### Environment Variables

1. `ETH_PROVIDER_URL` - A url for an Ethereum rpc provider. You will likely want to use a node that can withstand a decent amount of load and not inadvertantly abuse a public node.
2. `MONGO_HOST` - The URL to a MongoDB cluster (assumes mongo+srv:// url format)
3. `MONGO_PORT` the port on which to access your mongo cluster.
4. `MONGO_USER` the user created for the application to write to the database. 
5. `MONGO_PW` - Be careful with your password. 
6. `USER_AGENT_NAME` and `USER_AGENT_EMAIL` - Contact and identifying information to be provided to the catalyst nodes about who is connecting to them.
7. `IS_FOLLOWER` - Use this to set whether this is the main data collection agent (Leader) or a secondary agent used to collect data if the main agent goes offline.
### Additional Notes

When running this application we suggest not exposing any external access to the outside world; the application is not meant to service external API calls.

## DAO_DATA_QUERY

The DAO_DATA_QUERY application runs using docker or docker-compose. You will need to install the latest version of docker to deploy this application.

The application deploys one container which connects to your MongoDB (collected by the above DAO_DATA_COLLECT application) and provides endpoints that you or external users can use to access the application.

### Installing DAO_DATA_QUERY

**Note:** _Code for DAO_DATA_QUERY may be made available after the beta period as the calls and restrictions may change as we observe community use._

### Using DAO_DATA_QUERY

Users can access DAO_DATA_QUERY calls using the http request client of their choosing. Raw data endpoints support queries against the raw data and can be used as soon as data collection has begun. Additional queries support retrieval of derived data, computed by the `DAO_DATA_DERIVE` application.

### Query Documentation

Documentation for the currently supported list of DAO_DATA_QUERY calls can be found at https://dao-data.atlascorp.io/redoc. This list of queries may change over time but are built to return data from the MongoDB instance used to house data collected by the DAO_DATA_COLLECT application.

Additionally, a `/postman` call is included to obtain a document that can be imported into an instance of [Postman](https://www.postman.com/downloads/) to sample the API calls locally. 

## DAO_DATA_DERIVE

The DAO_DATA_DERIVE application runs using docker or docker-compose. You will need to install the latest version of docker to deploy this application.

The application deploys two containers - a python application with APIs to compute derived analytics and cache them back to the MongoDB instance, and a node-red cron to trigger the python APIs on a configurable schedule. Both of these applications connects to your MongoDB (collected by the above DAO_DATA_COLLECT application) and runs automatically to update data that is too cumbersome to request on demand.

### Installing DAO_DATA_DERIVE

**Note:** _Code for DAO_DATA_DERIVE_ may be made available after the beta period as the calls and restrictions may change as we observe community use._

### Using DAO_DATA_DERIVE

Users can access DAO_DATA_DERIVE data using the http request client of their choosing. In addition to the raw data endpoints supporting queries against the raw data, and  the DAO_DATA_QUERY application also supports retrieval of derived data computed by the `DAO_DATA_DERIVE` application.

### DAO_DATA_DERIVE Documentation

Documentation for the currently supported list of DAO_DATA_DERIVE calls can be found at https://dao-data.atlascorp.io/redoc. This list of derived data queries may change over time but are built to return data from the MongoDB instance used to house data collected by the DAO_DATA_COLLECT application and enhanced by the DAO_DATA_DERIVE application.

Additionally, a `/postman` call is included to obtain a document that can be imported into an instance of [Postman](https://www.postman.com/downloads/) to sample the API calls locally. 

## DAO_DATA_ARCHIVE

The DAO_DATA_ARCHIVE application runs using docker or docker-compose. You will need to install the latest version of docker to deploy this application.

The application deploys one container which connects to your MongoDB (collected by the above DAO_DATA_COLLECT application) and provides endpoints that you or external users can use to access the application.

### Installing DAO_DATA_ARCHIVE

The DAO_DATA_COLLECT application comes packaged with an installation routine. It can be run with or without using docker, but a docker-based installation is recommended as it offers standardization.
    
1. Copy the `example.env` in place file and rename it `.env`. Update each of the parameters in this file to the ones you will be using for your production application (e.g. your mongo instance details and your AWS S3 Bucket or similar S3 service).

2. Use docker to build the container using the command `docker build -t dao-data-archive .` in the project directory.

3. Deploy the docker container using your method of choice, such as docker using the command `docker run -p 4200:4200 dao-data-archive` or similar. The default port on which to run the application is `4200` but it can be altered within the code.

### Environment Variables

1. `MONGO_HOST` - The URL to a MongoDB cluster (assumes mongo+srv:// url format)
2. `MONGO_COLLECTION` - The collection name that houses the data. Defaults to "analytics".
3. `MONGO_USER` the user created for the application to write to the database. 
4. `MONGO_PW` - Be careful with your password. 
5. `AWS_ACCESS_KEY` - The access key used when connecting to your AWS S3 (or similar S3 cloud provider)
6. `AWS_SECRET_KEY` - The secret key to your cloud (AWS) S3 instance. Be careful with your secret keys.
7. `S3_BUCKET_NAME` - The name of the bucket on your S3 cloud instance in which the data will be stored. 

### Using DAO_DATA_ARCHIVE

Users can use the DAO_DATA_ARCHIVE application to import or export data from a MongoDB populated with data from an instance of DAO_DATA_COLLECT. The DAO_DATA_ARCHIVE application is configured to work with daily archive files, dictated by GMT time, in .tar format which can be housed in an S3 file system such as one hosted on AWS. 

**Note:** _We are currently populating an S3 repository with data archives dating back to 7/13/2022, but this data is not yet publicly available. We are still working on security and cost reduction measures and will likely open up this data source by the next DAO update. If you are interested in obtaining this data today, please reach out to `howie@atlascorp.io` for temporary access._

Alternatively, if you have access to a MongoDB with DAO_DATA_COLLECT data persisted, this application can be run to import/export data. 

The following endpoints are defined for convenience:

1. `POST /archive/export` - Exports data in 24-hour chunks by downloading (locally), compressing, and pushing to the defined S3 bucket. This application does remove files locally once processed. **Use POST to specify a start/end date as POST body parameters to restrict the backup process to a limited date range, as the GET method will export all data by default.**

2. `GET /archive/export` - Exports _the entire database contents_ in 24-hour chunks by downloading (locally), compressing, and pushing to the defined S3 bucket. This application does remove files locally once processed. Please be concious of data transfer charges when performing this operation.

3. `GET /archive/list` - Returns a list of all daily snapshots available in the defined S3 archive. Also creates temporary download links if you wish to retrieve the data manually.

4. `POST /archive/import` - Imports data housed on a cloud S3 instance to a defined MongoDB. **Use POST to specify a start/end date as POST body parameters to restrict the backup process to a limited date range, as the GET method will import all data by default.**

5. `GET /archive/import` - Imports all data housed on an S3 cloud instance to a defined MongoDB. A complete database import.

### Public Archive

Documentation for the currently supported list of DAO_DATA_QUERY calls can be found at https://dao-data.atlascorp.io/redoc. This list of queries may change over time but are built to return data from the MongoDB instance used to house data collected by the DAO_DATA_COLLECT application.

Additionally, a `/postman` call is included to obtain a document that can be imported into an instance of [Postman](https://www.postman.com/downloads/) to sample the API calls locally. 




