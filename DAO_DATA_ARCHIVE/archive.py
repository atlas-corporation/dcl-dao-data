from flask import Flask, jsonify, send_file, request
from flask_cors import CORS, cross_origin
import os
from datetime import datetime
import dateutil
from dotenv import load_dotenv
from subprocess import call
import boto3
import logging
import pymongo

logger = logging.getLogger(__name__)

print("Running DAO_DATA_ARCHIVE server powered by Atlas Corp")
app = Flask(__name__)
CORS(app)

#Global params
load_dotenv(os.path.join(os.path.dirname(__file__),".env"))
mongo_user = os.environ.get("mongo_user")
mongo_pw = os.environ.get("mongo_pw")
mongo_host = os.environ.get("mongo_host")
mongo_db = os.environ.get("mongo_db")
mongo_collection = os.environ.get("mongo_collection")
bucket_name = os.environ.get("bucket_name")
aws_access_key = os.environ.get("aws_access_key")
aws_secret_key = os.environ.get("aws_secret_key")

#Mongo Connect
client = pymongo.MongoClient("mongodb+srv://"+mongo_user+":"+mongo_pw+"@" + mongo_host +"/" + mongo_db)
db = client[mongo_db]

port= 4200
host='0.0.0.0'

session = boto3.Session(
    aws_access_key_id=aws_access_key,
    aws_secret_access_key=aws_secret_key,
)
s3 = session.resource('s3')

s3_client = boto3.client(
    's3',
    region_name='us-east-1',
    aws_access_key_id=aws_access_key,
    aws_secret_access_key=aws_secret_key,
)

#Note: Data in the database is in javascript format and therefore in Milliseconds. Python datetime uses seconds, hence many of the conversions below.

@app.route('/',methods=['GET','POST'])
def health():
    return jsonify("DAO Data Archive is up and running!")

@app.route('/archive/export',methods=['GET'])
@app.route('/archive/export',methods=['POST'])
def export_all():
    to_timestamp = None
    from_timestamp = None
    if request.method == "POST":
        req = request.json
        if 'start' in req and req['start']:
            try:
                from_timestamp = dateutil.parser.isoparse(req['start']).timestamp()
            except:
                return jsonify({'msg':'Malformed ISO Date for "start" time.'}),400
        else:
            return jsonify({'msg':'No start parameter included in the POST body. Please include "start" in ISO format.'}),400

        if 'end' in req and req['end']:
            try:
                to_timestamp = dateutil.parser.isoparse(req['end']).timestamp()
            except:
                return jsonify({'msg':'Malformed ISO Date for "end" time.'}),400
        else:
            return jsonify({'msg':'No end parameter included in the POST body. Please include "end" in ISO format.'}),400            
   
    if not from_timestamp:
        try:
            earliest_timestamp = list(db[mongo_collection].find({},{'timestamp':1,'_id':0}).sort("timestamp",1).limit(1))[0]['timestamp']/1000
        except:
            return jsonify("No data found in database, or mongo connection details misconfigured."),400
    else:
        earliest_timestamp = int(from_timestamp)

    if not to_timestamp:
        to_timestamp = (datetime.timestamp(int(datetime.now())))

    earliest_datetime = datetime.fromtimestamp(earliest_timestamp)
    from_timestamp = int(datetime.timestamp(datetime(earliest_datetime.year,earliest_datetime.month,earliest_datetime.day)))
    for t in range(from_timestamp,to_timestamp,(60 * 60 * 24)):
        print(str(datetime.fromtimestamp(t).date()),"Elapsed: " + str(int(datetime.timestamp(datetime.now()))-to_timestamp) + " seconds",flush=True)
        export(t*1000,t*1000+(1000 * 60 * 60 * 24))
    return jsonify("Database export complete!"),200

def export(from_timestamp,to_timestamp):
    #one day of data per archive
    print("Extracting data for " + str(datetime.fromtimestamp(from_timestamp/1000).date()) + " to " + str(datetime.fromtimestamp(to_timestamp/1000).date()),flush=True)
    call(r'''mongoexport --uri="mongodb+srv://'''+mongo_user+''':'''+mongo_pw+'''@'''+mongo_host+'''/" --db='''+mongo_db+''' --collection='''+mongo_collection+''' --query='{ "timestamp": { "$gte": '''+str(from_timestamp)+''', "$lt": '''+str(to_timestamp)+''' } }' --out=./'''+str(datetime.fromtimestamp(from_timestamp/1000).date())+'''.json''', shell=True)
    
    print("Compressing extracted data...",flush=True)
    call('tar cfz '+ str(datetime.fromtimestamp(from_timestamp/1000).date()) +'.tgz '+str(datetime.fromtimestamp(from_timestamp/1000).date())+'.json',shell=True)

    print("Deleting raw data...",flush=True)
    call('rm '+str(datetime.fromtimestamp(from_timestamp/1000).date())+'.json',shell=True)

    print("Uploading archive to AWS S3...",flush=True)
    upload_to_aws(from_timestamp)

    print("Removing compressed archive.",flush=True)
    call('rm '+str(datetime.fromtimestamp(from_timestamp/1000).date())+'.tgz',shell=True)

    return jsonify("Export complete!")

@app.route('/archive/list',methods=['GET','POST'])
def list_files():
    my_bucket = s3.Bucket(bucket_name)
    response = {'available-archives':[]}
    for my_bucket_object in my_bucket.objects.all():
        response['available-archives'].append({
            'date':my_bucket_object.key,
            'bucket':bucket_name,
            'size-MB':my_bucket_object.size/1024/1024,
            'download':s3_client.generate_presigned_url('get_object',
                        Params={'Bucket': bucket_name,
                                'Key': my_bucket_object.key},
                        ExpiresIn=3600)
            })
    return jsonify(response)

@app.route('/archive/import',methods=['GET'])
@app.route('/archive/import',methods=['POST'])
def import_all():
    from_date = datetime(1900,1,1).date()
    to_date = datetime.now().date()
    if request.method == "POST":
        req = request.json
        if 'start' in req and req['start']:
            try:
                from_date = dateutil.parser.isoparse(req['start']).date()
            except:
                return jsonify({'msg':'Malformed ISO Date for "start" time.'}),400

        if 'end' in req and req['end']:
            try:
                to_date = dateutil.parser.isoparse(req['end']).date()
            except:
                return jsonify({'msg':'Malformed ISO Date for "end" time.'}),400
      
    bucket = s3.Bucket(bucket_name)
    stopwatch = datetime.now().timestamp()
    for bucket_object in bucket.objects.all():
        data_date = datetime.strptime(bucket_object.key.split('.tar')[0], '%Y-%m-%d').date()
        if (data_date >= from_date) and (data_date <= to_date):

            print("Downloading file " + bucket_object.key,flush=True)
            download_file(bucket_object.key)
            
            print("Unzipping file...",flush=True)
            call('tar xvf '+ bucket_object.key,shell=True)
            
            print("Removing archive...",flush=True)
            call('rm '+ bucket_object.key,shell=True)

            print("Importing archive to mongo...",flush=True)
            call(r'''mongoimport --uri="mongodb+srv://'''+mongo_user+''':'''+mongo_pw+'''@'''+mongo_host+'''/" --db='''+mongo_db+''' --collection=islands --file '''+ bucket_object.key.split('.tar')[0]+'''.json''', shell=True)
            
            print("Removing unzipped archive...",flush=True)
            call('rm '+ bucket_object.key.split('.tar')[0]+'.json',shell=True)
            
            print("Time elapsed: " + str(datetime.now().timestamp() - stopwatch))

    return jsonify({'msg':"All files successfully imported!"}),200

def upload_to_aws(filename):
    s3.meta.client.upload_file(Filename=str(datetime.fromtimestamp(filename/1000).date())+'.tgz', Bucket=bucket_name, Key=str(datetime.fromtimestamp(filename/1000).date()) + '.tar')
    return "uploaded!"

def download_file(filename):
    s3_client.download_file(bucket_name,filename, './' + filename)
    return "downloaded!"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=port) 
