import os
import json
import requests
from web3 import Web3
from web3.middleware import geth_poa_middleware
from dotenv import load_dotenv

print("Setting up node-red environment with your details...",flush=True)

#pull in variables from .env file
load_dotenv(os.path.join(os.path.dirname(__file__),".env"))
MONGO_USER = mongo_pw = os.environ.get("MONGO_USER")
MONGO_PW = os.environ.get("MONGO_PW")
MONGO_HOST = os.environ.get("MONGO_HOST")
MONGO_PORT= os.environ.get("MONGO_PORT")
MONGO_DB_NAME = os.environ.get("MONGO_DB_NAME")
USER_AGENT_NAME = os.environ.get("USER_AGENT_NAME")
USER_AGENT_EMAIL = os.environ.get("USER_AGENT_EMAIL")
IS_FOLLOWER = os.environ.get("IS_FOLLOWER")

#Add your environment data to the templates
with open('./nodered/flows.template', 'r') as file :
  flow_file = file.read()

flow_file = flow_file.replace("{{MONGO_HOST}}", MONGO_HOST)
flow_file = flow_file.replace("{{MONGO_PORT}}", MONGO_PORT)
flow_file = flow_file.replace("{{USER_AGENT_NAME}}", USER_AGENT_NAME)
flow_file = flow_file.replace("{{USER_AGENT_EMAIL}}", USER_AGENT_EMAIL)

with open('./nodered/flows.json', 'w') as file:
  file.write(flow_file)

with open('./nodered/flows_cred.template','r') as file:
  cred_file = json.load(file)

cred_file['293124bc3c313541']['user'] = MONGO_USER
cred_file['293124bc3c313541']['password'] = MONGO_PW

with open('./nodered/flows_cred.json','w') as file:
  json.dump(cred_file,file)

print("Generating docker-compose.yml from current set of Catalyst Nodes...",flush=True)
#ETH node provider
providerURL = os.environ.get("ETH_NODE_RPC")
web3 = Web3(Web3.HTTPProvider(providerURL))
web3.middleware_onion.inject(geth_poa_middleware, layer=0)

#Set up access to ETH contract
catalystContract = "0x4a2f10076101650F40342885b99b6B101D83C486"
abi_file = './catalystABI.json'
with open(abi_file, 'r') as abi_definition:
    abi = json.load(abi_definition)
contract_address = Web3.toChecksumAddress(catalystContract)
contract = web3.eth.contract(address=contract_address, abi=abi)

print("   fetching catalyst node list from smart contract and names from node...",flush=True)
#Get total number of nodes to iterate over
numNodes = int(contract.functions.catalystCount().call())

#for each node get the ID and then use the ID to get the node details
catalyst_nodes = []
for n in range(0,numNodes):
    catalyst_id = contract.functions.catalystIds(n).call()
    catalyst_info = contract.functions.catalystById(catalyst_id).call()
    catalyst = {
        "owner":catalyst_info[1],
        "domain":catalyst_info[2],
        "start":catalyst_info[3],
        "end":catalyst_info[4]
    }
    #this doesn't always work, but when it does it's nice to have
    try:
        catalyst['name'] = requests.get('https://' + catalyst['domain'] + "/comms/status").json()['name']
    except:
        catalyst['name'] = catalyst['domain'].replace(".","-")
    catalyst_nodes.append(catalyst)

with open('catalyst_nodes.json', 'w') as f:
    json.dump(catalyst_nodes, f)

print("   building docker-compose.yml...",flush=True)
#create docker-compose.yml
docker_compose_string = '''
version: '3'
services:'''
#one docker image per catalyst node
for node in catalyst_nodes:
    docker_compose_string += '''
    '''+str(node['name'])+''':
      image: dclnodered
      build: ./nodered
      restart: unless-stopped
      environment:
        - realm='''+str(node['name'])+'''
        - node=https://'''+str(node['domain'])+'''
        - is_follower='''+str(IS_FOLLOWER)+'''
      networks:
        - appServer'''
docker_compose_string += '''
networks:
  appServer:
    driver: 'bridge'
'''

with open("docker-compose.yml", mode = "w") as g:
    g.write(docker_compose_string)

print("DAO-Data collector installed. Have a nice day!",flush=True)